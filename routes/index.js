var express = require('express')
	, router = express.Router()
	, api = require('../lib/open-weather-map')
	, _ = require('../lib/util');

// Frontpage with the widget
router.get('/', function(req, res, next) {
	let city = req.query.city || 'Copenhagen';

	api.getWeather(city).then(data => {
		res.render('index', data);
	}).catch(e => {
		res.render('index', e)
	})

});

// Return only the widget html
router.get('/widget', function(req, res, next) {
	let city = req.query.city || 'Copenhagen';

	api.getWeather(city).then(data => {
		res.render('components/widget', data, (err, html) => {
			res.json({
				city: data.city,
				widget: html
			})
		});
	}).catch(e => {
		res.json(e)
	})

});

module.exports = router;
