# Weather Widget
As a test for the TV2, Web Developer position
By Andreas Poulsen (andreas@poulsen.io)

## Tech
* NodeJS (>=7.0 should be alright) as core
* Express as Webserver
* Pug/Jade as template engine
* ES7 as front end JS
* Mocha and Chai as testing framework


## Installation
Install dependencies

`$ npm install`

Run webserver

`$ npm start`

It will open the app at http://localhost:3000, tested on Win/Chrome 66


## Testing
Make sure mocha is installed globally

`$ npm install mocha -g`

To run test

`$ npm test`

