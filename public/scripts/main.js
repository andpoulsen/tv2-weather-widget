(function() {
	let q = document.querySelector.bind(document)

	// Fetch and update widget on form submit
	document.forms['search'].addEventListener('submit', getWeatherData)
	// Update widget when going back and forth in history
	window.addEventListener('popstate', handleState)

	// Update initial page state
	history.replaceState(getState(), null, location.href)



	function getWeatherData(e) {
		e.preventDefault();
		var cityInput = e.srcElement.elements['city'].value
		cityInput = encodeURIComponent(cityInput.replace(/ /g, '+'))

		fetch(`/widget?city=${cityInput}`)
			.then(res => res.json())
			.then(res => {
				if(res.error)
					return setError(res)

				document.title = res.city + ' Weather'
				q('.widget-container').innerHTML = res.widget
				setError(false)
				history.pushState(getState(), null, `/?city=${cityInput}`)
			});

	}

	function getState() {
		return {
			widget: q('.widget-container').innerHTML,
			title: document.title
		}
	}

	function handleState(e) {
		if(e.state && e.state.widget) {
			q('.widget-container').innerHTML = e.state.widget
			document.title = e.state.title
		}
	}

	function setError(error) {
		var helpBlockDom = q('form[name=search] .help-block');
		helpBlockDom.classList.add('hide')
		if (error) {
			helpBlockDom.innerText = error.message
			helpBlockDom.classList.remove('hide')
		}
		q('body > .alert').classList.add('hide')
	}

})()