var assert = require('chai').assert
	, expect = require('chai').expect
	,	_ = require('../lib/util.js');

describe('util.js', function() {
  describe('#windDirection()', function() {
  	var tests = [
  	[350, 'North'],
  	[45, 'North East'],
  	[180, 'South']
  	];

  	tests.forEach(test => {
  		it(`should return ${test[1]}`, () => {
  			expect(_.windDirection(test[0])).to.be.equal(test[1]);
  		})
  	})

  });

  describe('#formatWeatherData()', function() {
  	var mock_res = {"coord":{"lon":12.57,"lat":55.69},"weather":[{"id":701,"main":"Mist","description":"mist","icon":"50n"},{"id":500,"main":"Rain","description":"light rain","icon":"10n"}],"base":"stations","main":{"temp":14.51,"pressure":1013,"humidity":100,"temp_min":14,"temp_max":15},"visibility":4000,"wind":{"speed":4.1,"deg":280},"clouds":{"all":75},"dt":1525996200,"sys":{"type":1,"id":5245,"message":0.0033,"country":"DK","sunrise":1526007984,"sunset":1526065625},"id":2618425,"name":"Copenhagen","cod":200};

  	var formatted = _.formatWeatherData(mock_res);

  	it('should format temperature as 14.51 °C', () => {
  		expect(formatted.conditions.temperature).to.be.equal('14.51 °C');
  	})
  	it('should format humidity as 100%', () => {
  		expect(formatted.conditions.humidity).to.be.equal('100%');
  	})
  	it('should format wind as 4.1 m/s West', () => {
  		expect(formatted.conditions.wind).to.be.equal('4.1 m/s West');
  	})
  	it('should format sunrise as 5:06', () => {
  		expect(formatted.conditions.sunrise).to.be.equal('5:06');
  	})

  });
});
