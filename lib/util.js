var moment = require('moment');

// Formats a timestamp to hours and minutes
function formatToTime(timestamp) {
	var m = moment(timestamp);

	// If seconds instead of milliseconds.
	if (m.isSame('1970-01-01', 'year'))
		m = moment(timestamp * 1000);

	return m.format('H:mm');
}

// Translates degrees to a cardinal direction
function windDirection(deg) {
	var dirs = ['North', 'North East', 'East', 'South East', 'South', 'South West', 'West', 'North West', 'North']
	return dirs[Math.round((deg % 360) / 45)];
}

// Format the raw weather data to a more renderable format
function formatWeatherData(d) {
	return {
		city: d.name,
		country: d.sys.country,
		description: d.weather[0].description,
		icon: 'http://openweathermap.org/img/w/' + d.weather[0].icon + '.png',
		conditions: {
			temperature: `${d.main.temp} °C`,
			humidity: `${d.main.humidity}%`,
			wind: `${d.wind.speed} m/s ` + windDirection(d.wind.deg),
			sunrise: formatToTime(d.sys.sunrise),
			sunset: formatToTime(d.sys.sunset),
		}
	}
}

module.exports = {
	formatToTime: formatToTime,
	windDirection: windDirection,
	formatWeatherData: formatWeatherData
};
