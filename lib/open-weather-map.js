var request = require('request-promise-native')
	, _ = require('./util');

const url = 'http://api.openweathermap.org/data/2.5/weather',
			key = '166d00e26d3ff2c6149e89feccc5c59a';

// Fetches the weather from open-weather-map api
function getWeather(city) {
	return new Promise((resolve, reject) => {

		let options = {
			url: url,
			qs: {
				q: city,
				appid: key,
				units: 'metric'
			},
			json: true
		}

		request(options)
			.then(d => {
				resolve(_.formatWeatherData(d))
			})
			.catch(e => {
				if (e.statusCode === 404)
					reject({error: true, message: `${city} not found` })
				else
					reject({error: true, message: e.error.message })
			})

	})
}

module.exports = {
	getWeather: getWeather
}